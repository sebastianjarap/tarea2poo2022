
import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<Lamp>();
        rollerShades = new ArrayList<DomoticDevice>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public void addRollerShade(RollerShade rs){
        rollerShades.add(rs);
    }

    public void startShadeUp(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.startUp();
            }
        }
    }
    public void startShadeDown(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.startDown();
            }
        }
    }

    public void stopShade(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.stop();
            }
        }
    }

    public Lamp getLampAtChannel( int channel){
        for (Lamp l: lamps)
            if (l.getChannel() ==channel)
                return l;
        return null;
    }
    public void changeLampPowerState(int channel){
        Lamp l=getLampAtChannel(channel);
        if (l != null) l.changePowerState();
    }

    public LampState getLampState(int channel){
        return lamps.get(0).getState();
    }

    public void changeRGB(int r, int g, int b){
        lamps.get(0).changeRGB(r,g,b);
    }


    private ArrayList<Lamp> lamps;
    private ArrayList<DomoticDevice> rollerShades;
}
