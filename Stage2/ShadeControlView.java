
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;


public class ShadeControlView extends BorderPane {
    public ShadeControlView (ShadeControl sc){
        Image flechaUp = new Image("Flecha.jpg");
        ImageView view = new ImageView(flechaUp);
        Image flechaDown = new Image("Flecha.jpg");
        ImageView view2 = new ImageView(flechaDown);
        Image flechaLeft = new Image("Flecha.jpg");
        ImageView view3 = new ImageView(flechaLeft);
        Image flechaRight = new Image("Flecha.jpg");
        ImageView view4 = new ImageView(flechaRight);

        ShadeControl shadeControl = sc;
        Button btnUp = new Button();
        Button btnDown = new Button();
        Button btnLeft = new Button();
        Button btnRight = new Button();
        Button channelButton = new Button("0");


        btnUp.setGraphic(view);
        view.setRotate(90);
        btnDown.setGraphic(view2);
        view2.setRotate(-90);
        btnLeft.setGraphic(view3);
        btnRight.setGraphic(view4);
        view4.setRotate(180);

        btnUp.setOnAction( e-> {
            if (getActualChannel() == sc.getChannel()) {
                sc.startUp();
                System.out.println("Cortina subiendo");
            }else {
                System.out.println("No hay roller en este canal");
            }
        });

        channelButton.setOnAction( e-> {
            if (getActualChannel() == sc.getChannel()) {
                sc.stop();
                System.out.println("Cortina stopped");
            }else {
                System.out.println("No hay roller en este canal");
            }
        });
        btnDown.setOnAction( e-> {
            if (getActualChannel() == sc.getChannel()) {
                sc.startDown();
                System.out.println("Cortina bajando");
            }else {
                System.out.println("No hay roller en este canal");
            }
        });
        btnLeft.setOnAction( e-> {
            a = getActualChannel();
            if(a > 0){
                this.a--;
            }else {
                this.a = getActualChannel();
            }
            System.out.println("Canal de el Roller: " + getActualChannel());
            channelButton.setText(String.valueOf(getActualChannel()));
        });

        btnRight.setOnAction( e-> {
            a = getActualChannel();
                if(a < sc.getChannel()){
                    this.a++;
                }else {
                    this.a = getActualChannel();
                }
            System.out.println("Canal de el Roller: " + getActualChannel());
            channelButton.setText(String.valueOf(getActualChannel()));
        });

        GridPane gridPane = new GridPane();
        gridPane.add(btnUp,1,0);
        gridPane.add(channelButton,1,1);
        gridPane.setHalignment(channelButton, HPos.CENTER);
        gridPane.add(btnDown,1,2);
        gridPane.add(btnLeft,0,1);
        gridPane.add(btnRight,2,1);

        setCenter(gridPane);

    }

    public int getActualChannel(){
        return a;
    }

    private int a;
}
