
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;

public class LampView extends Group {
    public LampView () {
        Polygon base = new Polygon();
        base.getPoints().addAll(new Double[]{
                3*18d, 3*20d,
                3*18d, 3*50d,
                3*13d, 3*50d,
                3*10d, 3*53d,
                3*10d, 3*60d,
                3*30d, 3*60d,
                3*30d, 3*53d,
                3*27d, 3*50d,
                3*22d, 3*50d,
                3*22d, 3*20d
        });

        base.setFill(Color.BLUE);

        lampshade = new Polygon();
        lampshade.getPoints().addAll(new Double[]{
                3*10d, 3*0d, //esquina superior izquierda
                3*0d, 3*20d, //esquina inferior izquierda
                3*40d, 3*20d,//esquina inferior derecha
                3*30d, 3*0d, //esquina superior derecha
        });

        lampshade.setFill(Color.WHITE);

        Polyline topContorno = new Polyline();
        topContorno.getPoints().addAll(new Double[]{
                3*10d, 3*0d, //esquina superior izquierda
                3*0d, 3*20d, //esquina inferior izquierda
                3*40d, 3*20d,//esquina inferior derecha
                3*30d, 3*0d, //esquina superior derecha
                3*10d, 3*0d, //linea superior
        });

        topContorno.setFill(Color.rgb(0,0,0));

        getChildren().addAll(topContorno, lampshade, base);
    }
    public void setColor(short r, short g, short b){
        lampshade.setFill(Color.rgb(r, g, b));
    }
    private Polygon lampshade;
}
