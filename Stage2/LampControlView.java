
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;


public class LampControlView extends VBox {
    public LampControlView(LampControl lampControl) {
        Image img = new Image("OFF.jpg");
        ImageView view = new ImageView(img);
        Image img2 = new Image("ON.jpg");
        ImageView view2 = new ImageView(img2);

        Button btnOnOFF = new Button();
        btnOnOFF.setGraphic(view);

        Spinner spinner = new Spinner(0, lampControl.getChannel(), 0);
        spinner.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);

        System.out.println("Estado: " + lampControl.getState(lampControl.getChannel()));

        Slider sliderR = new Slider(0, 255, 0);
        Label R = new Label("R");
        Label RLevel = new Label();
        RLevel.textProperty().bind(Bindings.format("%.0f", sliderR.valueProperty()));

        sliderR.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                if((int)spinner.getValue() == lampControl.getChannel()){
                    if(lampControl.getState(lampControl.getChannel())==LampState.ON){
                        r = new_val.intValue();
                        lampControl.changeRGB(r,g,b);
                    }else {
                        r = new_val.intValue();
                    }
                }
            }
        });

        r = (int) sliderR.getValue();

        Slider sliderG = new Slider(0, 255, 0);
        Label G = new Label("G");
        Label GLevel = new Label();
        GLevel.textProperty().bind(Bindings.format("%.0f", sliderG.valueProperty()));

        sliderG.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                if((int)spinner.getValue() == lampControl.getChannel()){
                    if(lampControl.getState(lampControl.getChannel())==LampState.ON){
                        g = new_val.intValue();
                        lampControl.changeRGB(r,g,b);
                    }else {
                        g = new_val.intValue();
                    }
                }
            }
        });

        g = (int) sliderG.getValue();

        Slider sliderB = new Slider(0, 255, 0);
        Label B = new Label("B");
        Label BLevel = new Label();
        BLevel.textProperty().bind(Bindings.format("%.0f", sliderB.valueProperty()));

        sliderB.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                if((int)spinner.getValue() == lampControl.getChannel()){
                    if(lampControl.getState(lampControl.getChannel())==LampState.ON){
                        b = new_val.intValue();
                        lampControl.changeRGB(r,g,b);
                    }else {
                        b = new_val.intValue();
                    }
                }
            }

        });
        b = (int) sliderR.getValue();

        btnOnOFF.setOnAction(event -> {
            if(lampControl.getChannel() == getSpinnerValue()) {
                if (lampControl.getState(lampControl.getChannel()) == LampState.ON) {
                    btnOnOFF.setGraphic(view);
                    lampControl.pressPower();
                    lampControl.changeRGB(255, 255, 255);
                } else if (lampControl.getState(lampControl.getChannel()) == LampState.OFF) {
                    btnOnOFF.setGraphic(view2);
                    lampControl.pressPower();
                    lampControl.changeRGB(getR(), getG(), getB());
                }

                System.out.println("Estado: " + lampControl.getState(lampControl.getChannel()));
                System.out.println("R: " + getR());
                System.out.println("G: " + getG());
                System.out.println("B: " + getB());
                System.out.println("canal: " + lampControl.getChannel());
            }else {
                System.out.println("No hay lamparas en canal " + getSpinnerValue());
            }
        });

        spinner.setOnMouseClicked(event1 -> {
            n = (int) spinner.getValue();
            if (lampControl.getChannel() != getSpinnerValue()){
                lampControl.changeRGB(255, 255, 255);
                btnOnOFF.setGraphic(view);
                sliderR.setValue(0);
                sliderG.setValue(0);
                sliderB.setValue(0);
            }else{
                sliderR.setValue(getR());
                sliderG.setValue(getG());
                sliderB.setValue(getB());
                if(lampControl.getState(lampControl.getChannel())==LampState.OFF){
                    lampControl.changeRGB(255, 255, 255);
                }else {
                    btnOnOFF.setGraphic(view2);
                    lampControl.changeRGB(getR(), getG(), getB());
                }
            }
        });

        GridPane gridPane = new GridPane();
        gridPane.add(btnOnOFF,1,0);
        gridPane.setHalignment(btnOnOFF, HPos.CENTER);

        gridPane.add(R,0,1);
        gridPane.add(G,0,2);
        gridPane.add(B,0,3);

        gridPane.add(sliderR,1,1);
        gridPane.add(sliderG,1,2);
        gridPane.add(sliderB,1,3);

        gridPane.add(RLevel,2,1);
        gridPane.add(GLevel,2,2);
        gridPane.add(BLevel,2,3);

        gridPane.add(spinner,1,5);

        getChildren().addAll(gridPane);
    }

    public int getR(){
        return r;
    }

    public int getG(){
        return g;
    }

    public int getB(){
        return b;
    }

    public int getSpinnerValue(){
        return n;
    }
    private int r =255,g=255,b=255;
    private int n;
}