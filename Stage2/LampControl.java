
import javafx.scene.layout.Pane;

public class LampControl extends DomoticDeviceControl{
    public LampControl(int channel, Cloud c){
        super(channel,c);
        cloud =c;
        view = new LampControlView(this);
    }
    public void pressPower(){
        cloud.changeLampPowerState(super.getChannel());
    }
    public Pane getView() {
        return view;
    }

    public LampState getState(int channel){
        return cloud.getLampState(channel);
    }
    public void changeRGB(int r,int g,int b){
        cloud.changeRGB(r,g,b);
    }

    private int channel;
    private Cloud cloud;
    private Pane view;
}
