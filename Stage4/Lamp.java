import javafx.scene.Node;

public class Lamp extends DomoticDevice{
    public Lamp (int channel){
    super(channel);
    r = 255;
    g = 255;
    b = 255;
    state = LampState.OFF;
    view = new LampView();
}
    public void changePowerState(){
        if (state==LampState.OFF) {
            state=LampState.ON;
            view.setColor(r,g,b);
        }else{
            state=LampState.OFF;
            view.setColor((short)255,(short)255, (short)255);
        }
    }
    public void changeRGB(int r, int g, int b){
        view.setColor((short) r, (short) g, (short) b);
    }
    public Node getView() {
        return view;
    }

    public LampState getState() {
        return state;
    }

    private short r,g,b;
    private LampState state;
    private LampView view;
}
