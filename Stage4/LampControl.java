import javafx.scene.layout.Pane;

import java.util.ArrayList;

public class LampControl extends DomoticDeviceControl{
    public LampControl(int channel, Cloud c){
        super(channel,c);
        cloud =c;
        view = new LampControlView(this);
    }
    public void pressPower(){
        cloud.changeLampPowerState(super.getChannel());
    }
    public Pane getView() {
        return view;
    }
    public LampState getState(int channel){
        return cloud.getLampState(channel);
    }
    public void changeRGB(int channel, int r,int g,int b){
        cloud.changeRGB(channel,r,g,b);
    }
    public ArrayList<DomoticDeviceControl> getLampsControls(){
        return cloud.getLampControls();
    }
    public ArrayList<DomoticDevice> getLampsCant(){
        return cloud.getLamps();
    }
    private Cloud cloud;
    private Pane view;
}
