import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.awt.*;

public class Stage4 extends Application {
    public void start(Stage primaryStage) {
        int lamp1Channel = 2;
        int lamp2Channel = 3;
        Cloud cloud = new Cloud();
        DomoticDevice lamp1 = new Lamp(lamp1Channel);
        DomoticDevice lamp2 = new Lamp(lamp2Channel);
        cloud.addLamp((Lamp) lamp1);
        cloud.addLamp((Lamp) lamp2);

        DomoticDeviceControl lampControl = new LampControl(lamp1Channel,cloud);
        DomoticDeviceControl lampControl2 = new LampControl(lamp2Channel,cloud);
        cloud.addLampControls(lampControl);
        cloud.addLampControls(lampControl2);

        HBox hBox = new HBox(20);
        hBox.setPadding(new Insets(20));
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(((Lamp) lamp1).getView(), ((Lamp) lamp2).getView(), ((LampControl) lampControl).getView());

        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(20));
        pane.setBottom(hBox);

        int shadeChannel = 2;
        int shade2Channel = 1;
        RollerShade rs = new RollerShade(shadeChannel, 2, 150, 100,"C:/Users/Agustin/Desktop/USM/Semestre 7/POO/TareaPOO/Stage4FX/src/main/java/image/fondoplaya.jpg");
        RollerShade rs2 = new RollerShade(shade2Channel, 2, 150, 100, "C:/Users/Agustin/Desktop/USM/Semestre 7/POO/TareaPOO/Stage4FX/src/main/java/image/Flecha.jpg");

        cloud.addRollerShade(rs);
        cloud.addRollerShade(rs2);

        HBox hbox2 = new HBox(20);
        hbox2.getChildren().addAll(rs2.getView(),rs.getView());
        pane.setCenter(hbox2);

        ShadeControl shadeControl = new ShadeControl(shadeChannel,cloud);
        ShadeControl shadeControl2 = new ShadeControl(shade2Channel,cloud);
        cloud.addShadeControls(shadeControl);
        cloud.addShadeControls(shadeControl2);

        hBox.getChildren().add(0,shadeControl.getView());

        Scene scene = new Scene(pane, 750, 400);
        primaryStage.setTitle("Domotic Devices Simulator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
