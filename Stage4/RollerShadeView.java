import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

public class RollerShadeView extends Group {
    public RollerShadeView(double maxLength, double width, double length, double radius, Color color) {
        Image img = new Image("image/fondoplaya.jpg");

        Rectangle background = new Rectangle(5,5, width-10,maxLength-5);
        background.setFill(new ImagePattern(img));  // I chose Blue

        getChildren().addAll(background);

        cloth = new Rectangle (0,1,width,length);
        cloth.setFill(color);
        cloth.setHeight(length+5);

        // Rolled up shade cloth
        Ellipse rightSide = new Ellipse(0, radius,radius/2,radius);
        rightSide.setFill(color);
        rightSide.setStroke(Color.BLACK);

        getChildren().addAll(cloth,rightSide);
    }
    public void setLength(double length) {
            cloth.setHeight(length);
    }
    private Rectangle cloth;
}
