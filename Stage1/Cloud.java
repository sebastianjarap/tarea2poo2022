import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<Lamp>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public Lamp getLampAtChannel( int channel){
        for (Lamp l: lamps)
            if (l.getChannel() ==channel)
                return l;
        return null;
    }
    public void changeLampPowerState(int channel){
        Lamp l=getLampAtChannel(channel);
        if (l != null) l.changePowerState();
    }

    public LampState getLampState(){
        return lamps.get(0).getState();
    }

    public void changeRGB(int r, int g, int b){
        lamps.get(0).changeRGB(r,g,b);
    }


    private ArrayList<Lamp> lamps;
}
