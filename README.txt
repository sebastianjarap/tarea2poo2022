Este proyecto tiene como finalidad modelar objetos reales como objetos de software, 
utilizando la programacion orientada a eventos para la creación de
un sistema domotico al programar y modelar cierto tipo de lamparas y 
de cortinas motorizadas.


Especificaciones.

Para un correcto funcionamiento del programa como equipo asumimos las siguentes precisiones:

-Todas las abreviaciones de los parámetros o atributos vienen del idioma inglés.
-El estado inicial para las lamparas es apagado y al encenderlas parten con
su intensidad al máximo.
-No es posible superar la máxima intensidad de las lamparas, o de la misma forma 
reducir la mínima intensidad de estas.
-Del mismo modo que en las lamparas, para el caso de las cortinas tampoco es viable
estirar o enrollar mas de lo posible este tipo de objetos.
-La nube funciona como un intermediario entre los controles y los objetos.
-Para el MakeFile, se necesita cambiar la variable ROUTEFX(ruta de libreria de javaFx) para el
correcto funcionamiento, la que esta en este momento es un ejemplo de ruta.


instrucciones de compilado y ejecución.

Dentro de la carpeta donde se ubica este archivo, se encuentra un fichero con el kit 
de desarrollo JDK, por lo cual independiente del ordenador donde se quiera ejecutar 
este simulador, la dirección/path de la variable de entorno del JDK siempre será 
la misma y no será necesario variar los comandos del MakeFile.

EL programa puede ser compilado y ejecutado en cada una de sus 4 etapas, obteniendo
así la solucion para cada subconjunto de los requerimientos pedidos. 
Cada etapa es compilada y ejecutada de la misma manera, hay que realizar 
los siguientes comandos al estar dentro de una terminal ubicada en la carpeta donde se 
encuentre este proyecto:
	>Make          #Para compilar el archivo
	>make run      #Para ejecutar el archivo  
	>make clear    #Para Eliminar el archivo ejecutable creado 


trabajo realizado por:
Sebastian andres jara palomino rol:201944515-4
Agustín Ovando Saenz rol: 201930544-1
& Benjamín Andrés Quiroz Aranguiz rol:201904509-1

Estudiantes de ingeniería civil telemática, UTFSM
