import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<RollerShade>();
        lampControls = new ArrayList<DomoticDeviceControl>();
    }

    public void addLamp(Lamp l) {
        lamps.add(l);
    }

    public void addRollerShade(RollerShade rs) {
        rollerShades.add(rs);
    }
    public void addLampControls(DomoticDeviceControl lc) {
        lampControls.add(lc);
    }
    public ArrayList<DomoticDevice> getLamps(){
        return lamps;
    }
    public ArrayList<DomoticDeviceControl> getLampControls(){
        return lampControls;
    }
    public void startShadeUp(int channel) {
        for (DomoticDevice dd : rollerShades) {
            RollerShade rs = (RollerShade) dd;
            if (dd.getChannel() == channel) {
                rs.startUp();
            }
        }
    }
    public void startShadeDown(int channel) {
        for (DomoticDevice dd : rollerShades) {
            RollerShade rs = (RollerShade) dd;
            if (dd.getChannel() == channel) {
                rs.startDown();
            }
        }
    }
    public void stopShade(int channel) {
        for (DomoticDevice dd : rollerShades) {
            RollerShade rs = (RollerShade) dd;
            if (dd.getChannel() == channel) {
                rs.stop();
            }
        }
    }
    public RollerShade.Motor.MotorState getGetMotorState(int channel) {
        for (DomoticDevice dd : rollerShades) {
            RollerShade rs = (RollerShade) dd;
            if (dd.getChannel() == channel) {
                return rs.getMotorState();
            }
        }
        return null;
    }

    public Lamp getLampAtChannel(int channel) {
        for (DomoticDevice dd : lamps)
            if (dd.getChannel() == channel) {
                return (Lamp) dd;
            }
        return null;
    }
    public void changeLampPowerState(int channel) {
        for (DomoticDevice dd : lamps) {
            Lamp l = (Lamp) dd;
            if (dd.getChannel() == channel) {
                l.changePowerState();
            }
        }
    }
    public LampState getLampState(int channel) {
        LampState state = null;
        for (DomoticDevice dd : lamps) {
            Lamp l = (Lamp) dd;
            if (dd.getChannel() == channel) {
                state = l.getState();
            } else ;
        }
        return state;
    }
    public void changeRGB(int channel, int r, int g, int b) {
        for (DomoticDevice dd : lamps) {
            Lamp l = (Lamp) dd;
            if (dd.getChannel() == channel) {
                if (getLampState(channel) == LampState.ON) {
                    l.changeRGB(r, g, b);
                }
            }
        }
    }
    private ArrayList<DomoticDevice> lamps;
    private ArrayList<RollerShade> rollerShades;
    private ArrayList<DomoticDeviceControl> lampControls;
}
