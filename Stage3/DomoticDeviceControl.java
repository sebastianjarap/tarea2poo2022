public class DomoticDeviceControl {
    public DomoticDeviceControl(int ch, Cloud c){
        channel = ch;
        cloud = c;
    }
    public int getChannel() {
        return channel;
    }
    public void setLampChannel(int ch){
        channel= ch;
    }
    protected Cloud cloud;
    private int channel;
}
